# Javascript Testing Framework

Tools used for testing:-
Jest (Test runner)
Enzyme (Testing Utility for React)
enzyme-to-json to convert Enzyme wrappers for Jest snapshot matcher. (Required for snapshot testing)

Installation:-

npm install --save-dev jest react-test-renderer enzyme enzyme-adapter-react-16 enzyme-to-json

Note:- Enzyme adapter should be installed as per compatible react version. Link for reference- https://github.com/airbnb/enzyme

Why Jest
* Very fast.
* Snapshot testing.
* Awesome interactive watch mode that reruns only tests that are relevant to your changes.
* Helpful fail messages.
* Simple configuration.
* Mocks and spies.
* Coverage report with a single command line switch.
* Obviously renowned to test React apps.

Why Enzyme
* Convenient utilities to work with shallow rendering, static rendered markup or DOM rendering.
* jQuery-like API to find elements, read props, etc.


Conventions:-

1. prop-Types can be used for type checking of props or to provide default props.
2. You can find Test File name conventions through this link — https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#filename-conventions


Note:-

-> Shallow Rendering renders only components itself with out its children. If a bug is introduced to child component then it won’t break your component’s test.
Shallow rendering is used for isolated unit tests
-> For components(parent-child) & props testing we have to use { mount } from enzyme and after assertion we can unmount the component.

